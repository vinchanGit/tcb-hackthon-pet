const cloud = require('wx-server-sdk')
cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database();

exports.main = async (event, context) => {
  console.log(event)
  switch (event.action) {
    case 'getBanner': {
      return getBanner(event)
    }
    case 'getLastRecord': {
      return getLastRecord(event)
    }
    case 'getNearbyRecord': {
      return getNearbyRecord(event)
    }
    case 'getHelp': {
      return getHelp(event)
    }
    default: {
      return
    }
  }
}

function getBanner(event) {
  let resp = { code: 0, msg: '', data: [] };
  //const geo = event.geo;
  const banner = [{
      id: 0,
      type: 'image',
      // appid: 'wxa233243843fef51d',
      // page: 'pages/homeTabBar/main',
      url: 'https://7065-pet-real-1301063915.tcb.qcloud.la/public/images/banner/banner0.png'
  }, {
      id: 1,
      type: 'image',
      url: 'https://7065-pet-real-1301063915.tcb.qcloud.la/public/images/banner/banner1.png',
  }, {
      id: 2,
      type: 'image',
      appid: '',
      page: '/pages/mine/help',
      url: 'https://7065-pet-real-1301063915.tcb.qcloud.la/public/images/banner/banner2.png'
    }];
  resp.code = 1;
  resp.data = banner;
  return resp;
}

async function getLastRecord(event) {
  let resp = { code: 0, msg: '', data: [] };
  // const geo = event.geo;
  // const userInfo = event.userInfo;
  // const openid = userInfo.openId;
  // const _ = db.command;
  let dbret = await db.collection('pet').where({
    // geo: _.geoNear({
    //   geometry: db.Geo.Point(geo.longitude, geo.latitude)
    // }),
    published: true,
    deleteTime:''
  }).orderBy('createTime', 'desc').limit(3).get();
  //console.log(dbret);
  let list = dbret.data;
  list.forEach((item) => {
    item.image = getCdnUrl(item.imgList[0]);
  })
  resp.code = 1;
  resp.data = list;
  return resp;
}

async function getNearbyRecord(event) {
  let resp = { code: 0, msg: '', data: [] };
  const geo = event.geo;
  //const userInfo = event.userInfo;
  //const openid = userInfo.openId;
  //console.log("geo:", geo)
  const _ = db.command
  let dbret = await db.collection('pet').where({
    geo: _.geoNear({
      geometry: db.Geo.Point(geo.longitude, geo.latitude)
    }),
    published: true,
    deleteTime:''
  }).orderBy('createTime', 'desc').limit(20).get();
  let list = dbret.data;
  let markers = [];
  list.forEach((item, index) => {
    let marker = {
      id :index,
      _id: item._id,
      latitude: item.location.latitude - ((item.createTime % 100) / 100000),
      longitude: item.location.longitude - ((item.createTime % 100) / 100000),
      width: 40,
      height: 40,
      iconPath: getCdnUrl(item.imgList[0]),
      petName: item.petName
    }
    markers.push(marker);
  })
  resp.code = 1;
  resp.data = markers;
  return resp;
}



function getHelp(event) {
  const list = [
    { q: '1.为什么我无法登录?', a: '请尝试升级微信版本后再重新登录。' },
    { q: '2.如何查看求助者真实号码?', a: '系统采用隐私号保护求助者号码信息，救助者点击"帮助TA"通过拨打隐私号与求助者联系。' },
    { q: '3.如何防止因救助发生纠纷?', a: '建议上门前救助者与救助者做好充分沟通，留下双方身份信息，或请物业保安陪同上门并录像。' },
    { q: '4.为什么求助信息未出现在首页?', a: '求助信息须审核通过后才能在首页展示，审核期间可以在"我的-求助记录"里查看与分享。' },

  ];
  return list;
}

function getCdnUrl(fileID) {
  const url = fileID ? fileID.replace(/cloud:\/\/pet-real.7065-pet-real-1301063915/, "https://7065-pet-real-1301063915.tcb.qcloud.la"):'';
  return url;
}
